/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.talos.training.constants;

/**
 * Global class for all Hybristraining constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class HybristrainingConstants extends GeneratedHybristrainingConstants
{
	public static final String EXTENSIONNAME = "hybristraining";

	private HybristrainingConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
