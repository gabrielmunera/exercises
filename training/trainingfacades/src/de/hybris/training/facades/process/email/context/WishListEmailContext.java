package de.hybris.training.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.training.core.model.process.WishListEmailProcessModel;

public class WishListEmailContext extends CustomerEmailContext {

    private String greeting;
    private String emailSubject;


    @Override
    public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
    {
        super.init(storeFrontCustomerProcessModel, emailPageModel);
        if (storeFrontCustomerProcessModel instanceof WishListEmailProcessModel)
        {
            setGreeting(((WishListEmailProcessModel) storeFrontCustomerProcessModel).getGreeting());
            setEmailSubject(((WishListEmailProcessModel) storeFrontCustomerProcessModel).getGreeting());
        }
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }
}
