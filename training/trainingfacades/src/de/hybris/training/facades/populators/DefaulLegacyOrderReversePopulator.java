package de.hybris.training.facades.populators;

import de.hybris.platform.commercefacades.order.data.LegacyOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.training.core.enums.LegacyOrderStatus;
import de.hybris.training.core.model.LegacyOrderModel;

public class DefaulLegacyOrderReversePopulator implements Populator<LegacyOrderData, LegacyOrderModel> {

    @Override
    public void populate(LegacyOrderData source, LegacyOrderModel target) throws ConversionException {
        target.setOrderNumber(source.getOrderNumber());
        target.setOrderStatus(LegacyOrderStatus.valueOf(source.getOrderStatus()));
        target.setOrderTotal(source.getOrderTotal());
    }
}
