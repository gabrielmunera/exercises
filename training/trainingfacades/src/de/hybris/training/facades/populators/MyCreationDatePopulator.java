package de.hybris.training.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.log4j.Logger;

public class MyCreationDatePopulator implements Populator<ProductModel, ProductData> {

    private static final Logger LOG = Logger.getLogger(MyCreationDatePopulator.class.getName());

    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        target.setMyCreationDate(source.getMyCreationDate());
        target.setPastDays(source.getPastDays());
        target.setCalculatedStock(source.getCalculatedStock());
        System.out.println("*********** my stock: " + source.getMyStock());
        LOG.info("*********** my stock: " + source.getMyStock());
        target.setMyStock(source.getMyStock());
    }
}
