package de.hybris.training.facades.populators;

import de.hybris.platform.commercefacades.order.data.LegacyOrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.training.core.model.LegacyOrderEntryModel;

public class DefaultLegacyOrderEntryPopulator implements Populator<LegacyOrderEntryModel, LegacyOrderEntryData> {

    private Converter<ProductModel, ProductData> productConverter;

    @Override
    public void populate(LegacyOrderEntryModel source, LegacyOrderEntryData target)
            throws ConversionException {

        target.setProduct(getProductConverter().convert(source.getProduct()));
        target.setBasePrice(source.getBasePrice());
        target.setQuantity(source.getQuantity());
    }

    public Converter<ProductModel, ProductData> getProductConverter() {
        return productConverter;
    }

    public void setProductConverter(Converter<ProductModel, ProductData> productConverter) {
        this.productConverter = productConverter;
    }
}
