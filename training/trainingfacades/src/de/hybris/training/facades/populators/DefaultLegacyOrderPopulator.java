package de.hybris.training.facades.populators;

import de.hybris.platform.commercefacades.order.data.LegacyOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.training.core.model.LegacyOrderModel;

public class DefaultLegacyOrderPopulator implements Populator<LegacyOrderModel, LegacyOrderData> {

    @Override
    public void populate(LegacyOrderModel source, LegacyOrderData target) throws ConversionException {
        target.setOrderNumber(source.getOrderNumber());
        target.setOrderStatus(source.getOrderStatus().toString());
        target.setOrderTotal(source.getOrderTotal());
    }
}
