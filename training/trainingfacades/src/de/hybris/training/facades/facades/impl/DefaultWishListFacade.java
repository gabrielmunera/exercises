package de.hybris.training.facades.facades.impl;

import de.hybris.platform.commerceservices.user.UserMatchingService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.training.core.services.WishListService;
import de.hybris.training.facades.facades.WishListFacade;
import org.springframework.util.Assert;

import java.util.Locale;

public class DefaultWishListFacade implements WishListFacade {

    private UserMatchingService userMatchingService;
    private WishListService wishListService;

    @Override
    public void sendWishListEvent(String email) {
        Assert.hasText(email, "The field [id] cannot be empty");
        final CustomerModel customerModel = getUserMatchingService()
                .getUserByProperty(email.toLowerCase(Locale.ENGLISH), CustomerModel.class);
        getWishListService().sendWishListEvent(customerModel);
    }

    public UserMatchingService getUserMatchingService() {
        return userMatchingService;
    }

    public void setUserMatchingService(UserMatchingService userMatchingService) {
        this.userMatchingService = userMatchingService;
    }

    public WishListService getWishListService() {
        return wishListService;
    }

    public void setWishListService(WishListService wishListService) {
        this.wishListService = wishListService;
    }
}
