package de.hybris.training.facades.facades;

import de.hybris.platform.commercefacades.order.data.LegacyOrderData;

import java.util.List;

public interface LegacyOrderFacade {

    List<LegacyOrderData> getLegacyOrdersByCurrentCustomer();

    LegacyOrderData findOrderByOrderNumber(Integer orderNumber);
}
