package de.hybris.training.facades.facades.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductVariantFacade;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.training.facades.facades.MyCreationDateFacade;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DefaultMyCreationDateFacade extends DefaultProductVariantFacade implements MyCreationDateFacade {

    @Override
    public ProductData updateMyCreationDate(String productCode, String myCreationDate) {
        ProductModel productModel = getProductService().getProductForCode(productCode);
        productModel.setMyCreationDate(convertCreationDateStringToDate(myCreationDate));
        getModelService().save(productModel);
        return getProductForCodeAndOptions(productCode, null);
    }

    private Date convertCreationDateStringToDate(String myCreationDate){

        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(myCreationDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Not a valid date " + myCreationDate);
        }
    }
}
