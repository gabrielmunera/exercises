package de.hybris.training.facades.facades;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.Date;

public interface MyCreationDateFacade {

    ProductData updateMyCreationDate(String productCode, String myCreationDate);
}
