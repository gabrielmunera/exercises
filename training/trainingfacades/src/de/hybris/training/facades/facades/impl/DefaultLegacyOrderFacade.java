package de.hybris.training.facades.facades.impl;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.LegacyOrderData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.training.core.model.LegacyOrderModel;
import de.hybris.training.core.services.LegacyOrderService;
import de.hybris.training.facades.facades.LegacyOrderFacade;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public class DefaultLegacyOrderFacade implements LegacyOrderFacade {

    private LegacyOrderService legacyOrderService;
    private CustomerFacade customerFacade;
    private Converter<LegacyOrderModel, LegacyOrderData> legacyOrderConverter;
    private Converter<CustomerData, CustomerModel> customerReverseConverter;

    @Override
    public List<LegacyOrderData> getLegacyOrdersByCurrentCustomer() {
        CustomerData customerData = customerFacade.getCurrentCustomer();
        CustomerModel customer = customerReverseConverter.convert(customerData);

        return getLegacyOrderConverter().convertAll(legacyOrderService.getLegacyOrdersByCurrentCustomer(customer));
    }

    public LegacyOrderData findOrderByOrderNumber(Integer orderNumber){
        List<LegacyOrderModel> legacyOrders = legacyOrderService.findOrderByOrderNumber(orderNumber);
        if(CollectionUtils.isEmpty(legacyOrders)){
            throw new IllegalArgumentException("No legacy order found by " + orderNumber);
        }
        return legacyOrderConverter.convert(legacyOrders.get(0));
    }

    public LegacyOrderService getLegacyOrderService() {
        return legacyOrderService;
    }

    public void setLegacyOrderService(LegacyOrderService legacyOrderService) {
        this.legacyOrderService = legacyOrderService;
    }

    public Converter<LegacyOrderModel, LegacyOrderData> getLegacyOrderConverter() {
        return legacyOrderConverter;
    }

    public void setLegacyOrderConverter(Converter<LegacyOrderModel, LegacyOrderData> legacyOrderConverter) {
        this.legacyOrderConverter = legacyOrderConverter;
    }

    public CustomerFacade getCustomerFacade() {
        return customerFacade;
    }

    public void setCustomerFacade(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    public Converter<CustomerData, CustomerModel> getCustomerReverseConverter() {
        return customerReverseConverter;
    }

    public void setCustomerReverseConverter(Converter<CustomerData, CustomerModel> customerReverseConverter) {
        this.customerReverseConverter = customerReverseConverter;
    }

}
