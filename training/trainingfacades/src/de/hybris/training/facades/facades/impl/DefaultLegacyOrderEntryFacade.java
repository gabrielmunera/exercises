package de.hybris.training.facades.facades.impl;

import de.hybris.platform.commercefacades.order.data.LegacyOrderEntryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.training.core.model.LegacyOrderEntryModel;
import de.hybris.training.core.model.LegacyOrderModel;
import de.hybris.training.core.services.LegacyOrderEntryService;
import de.hybris.training.core.services.LegacyOrderService;
import de.hybris.training.facades.facades.LegacyOrderEntryFacade;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public class DefaultLegacyOrderEntryFacade implements LegacyOrderEntryFacade {

    private LegacyOrderEntryService legacyOrderEntryService;
    private LegacyOrderService legacyOrderService;
    private Converter<LegacyOrderEntryModel, LegacyOrderEntryData> legacyOrderEntryConverter;

    @Override
    public List<LegacyOrderEntryData> getEntriesByOrder(Integer orderNumber) {
        LegacyOrderModel order = getLegacyOrderModel(orderNumber);
        List<LegacyOrderEntryModel> entriesModel = legacyOrderEntryService.getEntriesByOrder(order);
        return legacyOrderEntryConverter.convertAll(entriesModel);
    }

    private LegacyOrderModel getLegacyOrderModel(Integer orderNumber) {
        List<LegacyOrderModel> legacyOrders = legacyOrderService.findOrderByOrderNumber(orderNumber);
        if(CollectionUtils.isEmpty(legacyOrders)){
            throw new IllegalArgumentException("No legacy order found by order number" + orderNumber);
        }
        return legacyOrders.get(0);
    }

    public LegacyOrderEntryService getLegacyOrderEntryService() {
        return legacyOrderEntryService;
    }

    public void setLegacyOrderEntryService(LegacyOrderEntryService legacyOrderEntryService) {
        this.legacyOrderEntryService = legacyOrderEntryService;
    }

    public LegacyOrderService getLegacyOrderService() {
        return legacyOrderService;
    }

    public void setLegacyOrderService(LegacyOrderService legacyOrderService) {
        this.legacyOrderService = legacyOrderService;
    }

    public Converter<LegacyOrderEntryModel, LegacyOrderEntryData> getLegacyOrderEntryConverter() {
        return legacyOrderEntryConverter;
    }

    public void setLegacyOrderEntryConverter(Converter<LegacyOrderEntryModel, LegacyOrderEntryData> legacyOrderEntryConverter) {
        this.legacyOrderEntryConverter = legacyOrderEntryConverter;
    }
}
