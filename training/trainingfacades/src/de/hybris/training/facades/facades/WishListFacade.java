package de.hybris.training.facades.facades;

public interface WishListFacade {

    void sendWishListEvent(String email);
}
