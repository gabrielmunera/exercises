package de.hybris.training.facades.facades;

import de.hybris.platform.commercefacades.order.data.LegacyOrderEntryData;
import de.hybris.training.core.model.LegacyOrderModel;

import java.util.List;

public interface LegacyOrderEntryFacade {

    List<LegacyOrderEntryData> getEntriesByOrder(Integer orderNumber);
}
