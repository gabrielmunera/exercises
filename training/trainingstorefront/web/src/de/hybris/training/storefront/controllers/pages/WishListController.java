package de.hybris.training.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.training.facades.facades.WishListFacade;
import de.hybris.training.storefront.controllers.ControllerConstants;
import de.hybris.training.storefront.forms.WishListForm;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.validation.Valid;


@Controller
@Scope("tenant")
@RequestMapping("/my-account/wish-list")
public class WishListController extends AbstractPageController {

    private static final Logger LOG = Logger.getLogger(WishListController.class);

    private static final String WISH_LIST_CMS_PAGE = "wish-list";
    private static final String BREADCRUMBS_ATTR = "breadcrumbs";

    @Resource(name = "accountBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "wishListFacade")
    private WishListFacade wishListFacade;

    @RequestMapping(method = RequestMethod.GET)
    @RequireHardLogIn
    public String getCustomerInterests(final Model model)
            throws CMSItemNotFoundException
    {
        WishListForm wishListForm = new WishListForm();
        model.addAttribute("wishListForm", wishListForm);

        final ContentPageModel wishListPage = getContentPageForLabelOrId(WISH_LIST_CMS_PAGE);
        storeCmsPageInModel(model, wishListPage);
        setUpMetaDataForContentPage(model, wishListPage);
        model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.wishList"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public String passwordRequest(@Valid final WishListForm form, final BindingResult bindingResult, final Model model) {
        if (bindingResult.hasErrors())
        {
            return ControllerConstants.Views.Fragments.Password.PasswordResetRequestPopup;
        }
        else
        {
            try
            {
                wishListFacade.sendWishListEvent(form.getEmail());
            }
            catch (final UnknownIdentifierException unknownIdentifierException)
            {
                LOG.warn("Email: " + form.getEmail() + " does not exist in the database.");
                return "Error";
            }
            return "Okay";
        }
    }
}
