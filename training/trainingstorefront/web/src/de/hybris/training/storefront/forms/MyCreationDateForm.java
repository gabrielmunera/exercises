package de.hybris.training.storefront.forms;

import javax.validation.constraints.NotNull;

public class MyCreationDateForm {

    @NotNull(message = "{my.creation.date.not.empty}")
    private String myCreationDate;
    private String productCode;

    public String getMyCreationDate() {
        return myCreationDate;
    }

    public void setMyCreationDate(String myCreationDate) {
        this.myCreationDate = myCreationDate;
    }

    public void setProductCode(final String productCode)
    {
        this.productCode = productCode;
    }

    public String getProductCode()
    {
        return productCode;
    }
}
