ACC.legacyorder = {
    _autoload: [
        "testLoad"
    ],

    testLoad: function(){
        console.log("testLoad");
        $("#legacyOrderDetail").hide();
        $("#legacyOrder").show();
        $(document).on("click","#btnGoDetail", function(event){
            event.preventDefault();

            var searchUrl = $(this).data('searchUrl');

            $.ajax({
                url: ACC.config.encodedContextPath + searchUrl,
                type: 'GET',
                traditional: true,
                success: function (data) {
                    console.log("It worked")
                    $("#legacyOrder").hide();
                    $("#legacyOrderDetail").show();
                },
                error: function (xht, textStatus, ex) {
                    console.log("Failed to get delivery modes. Error details [" + xht + ", " + textStatus + ", " + ex + "]");   // NOSONAR
                }
            });
        });

        $(document).on("click","#btnGoBack", function(){

            $("#legacyOrder").show();
            $("#legacyOrderDetail").hide();
        });
    }
};