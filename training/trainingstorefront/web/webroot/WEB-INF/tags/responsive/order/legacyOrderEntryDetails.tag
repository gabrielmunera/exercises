<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.LegacyOrderData" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.LegacyOrderEntryData" %>
<%@ attribute name="itemIndex" required="true" type="java.lang.Integer" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="defaultViewConfigurationInfosBaseUrl" value="/my-account/legacy-order" />

<c:url value="${orderEntry.product.url}" var="productUrl"/>
<c:set var="entryStock" value="${fn:escapeXml(orderEntry.product.stock.stockLevelStatus.code)}"/>
<li class="item__list--item">
    <%-- chevron for multi-d products --%>
    <div class="hidden-xs hidden-sm item__toggle">
        <c:if test="${orderEntry.product.multidimensional}">
            <div class="js-show-multiD-grid-in-order" data-index="${itemIndex}">
                <ycommerce:testId code="cart_product_updateQuantity">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </ycommerce:testId>
            </div>
        </c:if>
    </div>

    <%-- product image --%>
    <div class="item__image">
        <ycommerce:testId code="orderDetail_productThumbnail_link">
            <a href="${fn:escapeXml(productUrl)}">
                <product:productPrimaryImage product="${orderEntry.product}" format="thumbnail"/>
            </a>
        </ycommerce:testId>
    </div>

        <%-- product name, code, promotions --%>
        <div class="item__info">
            <ycommerce:testId code="orderDetails_productName_link">
                <a href="${orderEntry.product.purchasable ? fn:escapeXml(productUrl) : ''}"><span class="item__name">${fn:escapeXml(orderEntry.product.name)}</span></a>
            </ycommerce:testId>

            <div class="item__code">
                <ycommerce:testId code="orderDetails_productCode">
                    ${fn:escapeXml(orderEntry.product.code)}
                </ycommerce:testId>
            </div>
        </div>

        <%-- price --%>
        <div class="item__price">
            <span class="visible-xs visible-sm"><spring:theme code="basket.page.itemPrice"/>:</span>
            <ycommerce:testId code="orderDetails_productItemPrice_label">
                ${orderEntry.basePrice}
            </ycommerce:testId>
        </div>

        <%-- quantity --%>
        <div class="item__quantity hidden-xs hidden-sm">
            ${fn:escapeXml(orderEntry.quantity)}
        </div>

        <%-- total --%>
        <div class="item__total hidden-xs hidden-sm">
            <ycommerce:testId code="orderDetails_productTotalPrice_label">
               ${order.orderTotal}
            </ycommerce:testId>
        </div>
</li>



