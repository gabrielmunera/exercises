<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.LegacyOrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="order-detail-overview">
    <div class="row">
        <div class="col-sm-3">
            <div class="item-group">
                    <span class="item-label"><spring:theme code="text.account.orderHistory.orderNumber"/></span>
                    <span class="item-value">${fn:escapeXml(legacyOrderData.orderNumber)}</span>
            </div>
        </div>
        <c:if test="${not empty legacyOrderData.orderStatus}">
            <div class="col-sm-3">
                <div class="item-group">
                        <span class="item-label"><spring:theme code="text.account.orderHistory.orderStatus"/></span>
                        <span class="item-value">${legacyOrderData.orderStatus}</span>
                </div>
            </div>
        </c:if>
        <div class="col-sm-3">
            <div class="item-group">
                    <span class="item-label"><spring:theme code="text.account.order.total"/></span>
                    <span class="item-value">${order.orderTotal}</span>
            </div>
        </div>
    </div>
</div>