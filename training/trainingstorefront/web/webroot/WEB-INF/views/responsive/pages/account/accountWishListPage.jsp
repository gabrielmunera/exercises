<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/my-account/wish-list/send" var="sendWishListEmailURL" htmlEscape="false" />

<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-6">
            <spring:theme code="text.account.wishList"/>
        </div>
    </div>
</div>

<div>
    <br class="control-group">
        <form:input path="wishListForm.email" type="text" value="${wishListForm.email}"
               class="form-control"
               id="customerEmailInput" name="customerEmailInput"/>
        </br>
        <button class="btn btn-primary btn-block"
                id="btnSendEmail"
                data-send-email-url="${fn:escapeXml(sendWishListEmailURL)}"
                value="Send">Send</button>
        </div>
</div>