ACC.wishlist = {
    _autoload: [
        "sendEmailListener"
    ],

    sendEmailListener: function(){
        console.log("sendEmailListener");

        $(document).on("click","#btnSendEmail", function(event){
            console.log("onSendEmail");

            //event.preventDefault();

            var customerEmail = $("#customerEmailInput").val();
            var sendEmailUrl = $(this).data('sendEmailUrl');

            var postData = {email: customerEmail};

            $.ajax({
                url: sendEmailUrl,
                type: 'POST',
                data: postData,
                traditional: true,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $("#customerEmailInput").val("");
                    alert(data);
                },
                error: function (xht, textStatus, ex) {
                    $("#customerEmailInput").val("");
                    console.log("Send email failed. Error details [" + xht + ", " + textStatus + ", " + ex + "]");   // NOSONAR
                }
            });
        });
    }
};