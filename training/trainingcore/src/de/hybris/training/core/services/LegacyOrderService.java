package de.hybris.training.core.services;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.training.core.model.LegacyOrderModel;

import java.util.List;

public interface LegacyOrderService {

    List<LegacyOrderModel> getLegacyOrdersByCurrentCustomer(CustomerModel customer);

    List<LegacyOrderModel> findOrderByOrderNumber(Integer oderNumber);
}
