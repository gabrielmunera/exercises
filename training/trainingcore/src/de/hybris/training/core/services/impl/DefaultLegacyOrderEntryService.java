package de.hybris.training.core.services.impl;

import de.hybris.training.core.dao.LegacyOrderEntryDAO;
import de.hybris.training.core.model.LegacyOrderEntryModel;
import de.hybris.training.core.model.LegacyOrderModel;
import de.hybris.training.core.services.LegacyOrderEntryService;

import java.util.List;

public class DefaultLegacyOrderEntryService implements LegacyOrderEntryService {

    private LegacyOrderEntryDAO legacyOrderEntryDAO;

    @Override
    public List<LegacyOrderEntryModel> getEntriesByOrder(LegacyOrderModel order) {
        return legacyOrderEntryDAO.getEntriesByOrder(order);
    }

    public LegacyOrderEntryDAO getLegacyOrderEntryDAO() {
        return legacyOrderEntryDAO;
    }

    public void setLegacyOrderEntryDAO(LegacyOrderEntryDAO legacyOrderEntryDAO) {
        this.legacyOrderEntryDAO = legacyOrderEntryDAO;
    }
}
