package de.hybris.training.core.services;

import de.hybris.training.core.model.LegacyOrderEntryModel;
import de.hybris.training.core.model.LegacyOrderModel;

import java.util.List;

public interface LegacyOrderEntryService {

    List<LegacyOrderEntryModel> getEntriesByOrder(LegacyOrderModel order);
}
