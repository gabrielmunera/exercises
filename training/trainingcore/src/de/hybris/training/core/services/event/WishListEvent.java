package de.hybris.training.core.services.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class WishListEvent extends AbstractCommerceUserEvent<BaseSiteModel> {

    private String greeting;

    public WishListEvent() {
    }

    public WishListEvent(String greeting) {
        this.greeting = greeting;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
}
