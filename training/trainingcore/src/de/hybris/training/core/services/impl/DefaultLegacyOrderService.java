package de.hybris.training.core.services.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.training.core.dao.LegacyOrderDAO;
import de.hybris.training.core.model.LegacyOrderModel;
import de.hybris.training.core.services.LegacyOrderService;

import java.util.List;

public class DefaultLegacyOrderService implements LegacyOrderService {

    private LegacyOrderDAO legacyOrderDao;

    @Override
    public List<LegacyOrderModel> getLegacyOrdersByCurrentCustomer(CustomerModel customer) {
        return legacyOrderDao.findAllLegacyOrdersByCustomer(customer);
    }

    @Override
    public List<LegacyOrderModel> findOrderByOrderNumber(Integer oderNumber) {
        return legacyOrderDao.findOrderByOrderNumber(oderNumber);
    }

    public LegacyOrderDAO getLegacyOrderDao() {
        return legacyOrderDao;
    }

    public void setLegacyOrderDao(LegacyOrderDAO legacyOrderDao) {
        this.legacyOrderDao = legacyOrderDao;
    }

}
