package de.hybris.training.core.services;

import de.hybris.platform.core.model.user.CustomerModel;

public interface WishListService {

    void sendWishListEvent(final CustomerModel customerModel);
}
