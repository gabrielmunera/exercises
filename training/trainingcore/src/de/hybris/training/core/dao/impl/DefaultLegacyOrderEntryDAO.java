package de.hybris.training.core.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.training.core.dao.LegacyOrderEntryDAO;
import de.hybris.training.core.model.LegacyOrderEntryModel;
import de.hybris.training.core.model.LegacyOrderModel;

import java.util.List;

public class DefaultLegacyOrderEntryDAO implements LegacyOrderEntryDAO {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<LegacyOrderEntryModel> getEntriesByOrder(LegacyOrderModel order) {
        String query = "SELECT {loe.PK} " +
                "FROM {LegacyOrderEntry AS loe JOIN LegacyOrder AS lo ON {lo.pk} = {loe.legacyOrder}} " +
                "WHERE {lo.orderNumber} = ?orderNumber";
        FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.addQueryParameter("orderNumber", order.getOrderNumber());
        SearchResult<LegacyOrderEntryModel> searchResult = flexibleSearchService.search(searchQuery);
        return searchResult.getResult();
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
