package de.hybris.training.core.dao;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.training.core.model.LegacyOrderModel;

import java.util.List;

public interface LegacyOrderDAO {
    List<LegacyOrderModel> findAllLegacyOrdersByCustomer(CustomerModel customer);
    List<LegacyOrderModel> findOrderByOrderNumber(Integer oderNumber);
}
