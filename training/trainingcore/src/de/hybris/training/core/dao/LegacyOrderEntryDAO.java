package de.hybris.training.core.dao;

import de.hybris.training.core.model.LegacyOrderEntryModel;
import de.hybris.training.core.model.LegacyOrderModel;

import java.util.List;

public interface LegacyOrderEntryDAO {
    List<LegacyOrderEntryModel> getEntriesByOrder(LegacyOrderModel model);
}
