package de.hybris.training.core.dao.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.training.core.dao.LegacyOrderDAO;
import de.hybris.training.core.model.LegacyOrderModel;

import java.util.List;

public class DefaultLegacyOrderDAO implements LegacyOrderDAO {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<LegacyOrderModel> findAllLegacyOrdersByCustomer(CustomerModel customer) {
        String query = "SELECT {lo.PK} " +
                "FROM {LegacyOrder AS lo JOIN Customer AS c ON {c.pk} = {lo.customer}} " +
                "WHERE {c.uid} = ?customerUid";
        FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.addQueryParameter("customerUid", customer.getOriginalUid());
        SearchResult<LegacyOrderModel> searchResult = flexibleSearchService.search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<LegacyOrderModel> findOrderByOrderNumber(Integer oderNumber) {
        String query = "SELECT {PK} " +
                "FROM {LegacyOrder AS lo} " +
                "WHERE {lo.orderNumber} = ?oderNumber";
        FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.addQueryParameter("oderNumber", oderNumber);
        SearchResult<LegacyOrderModel> searchResult = flexibleSearchService.search(searchQuery);
        return searchResult.getResult();
    }


    public FlexibleSearchService getFlexibleSearchService(){
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService){
        this.flexibleSearchService = flexibleSearchService;
    }
}
