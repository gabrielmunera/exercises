package de.hybris.training.core.poc;

import com.google.common.base.Preconditions;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobProperties;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import de.hybris.platform.azure.media.AzureCloudUtils;
import de.hybris.platform.azure.media.storage.WindowsAzureBlobStorageStrategy;
import de.hybris.platform.media.exceptions.ExternalStorageServiceException;
import de.hybris.platform.media.exceptions.MediaStoreException;
import de.hybris.platform.media.services.MediaLocationHashService;
import de.hybris.platform.media.services.impl.HierarchicalMediaPathBuilder;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.storage.impl.StoredMediaData;
import de.hybris.platform.util.MediaUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.InputStream;
import java.time.Duration;
import java.util.Map;

public class CustomWindowsAzureBlobStorageStrategy {

    private MediaLocationHashService locationHashService;

    private static final Logger LOG = LoggerFactory.getLogger(CustomWindowsAzureBlobStorageStrategy.class);

    public StoredMediaData store(MediaStorageConfigService.MediaFolderConfig config, String mediaId, Map<String, Object> metaData, InputStream dataStream) {
        Preconditions.checkArgument(config != null, "folder config is required!");
        Preconditions.checkArgument(mediaId != null, "mediaId is required!");
        Preconditions.checkArgument(metaData != null, "metaData is required!");
        Preconditions.checkArgument(dataStream != null, "dataStream is required!");
        Object size = metaData.get("size");
        String mime = (String)metaData.get("mime");
        Preconditions.checkArgument(size instanceof Long, "Object size as Long is required to store blobs in Azure Blob Storage. Only local caching allows to get size.");

        try {
            String location = this.assembleLocation(mediaId, (String)metaData.get("fileName"), (String)metaData.get("productId"));
            CloudBlockBlob blob = this.getBlockBlobReference(config, location);
            blob.upload(dataStream, (Long)size);
            BlobProperties props = blob.getProperties();
            props.setCacheControl("public, max-age=3600");
            if (metaData.get("mime") != null) {
                props.setContentType((String)metaData.get("mime"));
            }

            blob.uploadProperties();
            String blobName = blob.getName();
            String hashForLocation = this.locationHashService.createHashForLocation(config.getFolderQualifier(), blobName);
            return new StoredMediaData(blobName, hashForLocation, (Long)size, mime);
        } catch (Exception var12) {
            throw new MediaStoreException(var12);
        }
    }

    private String assembleLocation(String mediaId, String realFileName, String productId) {
        /* This is what adds the hex named folders */
        HierarchicalMediaPathBuilder pathBuilder = HierarchicalMediaPathBuilder.forDepth(2);
        String oldWayLocation = pathBuilder.buildPath((String)null, mediaId) + mediaId;

        /* Here we can apply whatever logic we want to assemble the location */
        String location = String.format("products/%s/%s", productId, mediaId);
        LOG.info("***** -> Old Way Location: {} <- *****", oldWayLocation);
        LOG.info("***** -> New Way Location: {} <- *****", location);
        if (StringUtils.isNotBlank(realFileName)) {
            location = MediaUtil.addTrailingFileSepIfNeeded(location) + realFileName;
        }

        return location.replaceAll("\\\\", "/");
    }

    private CloudBlockBlob getBlockBlobReference(MediaStorageConfigService.MediaFolderConfig config, String location) throws Exception {
        CloudBlobClient blobClient = this.getCloudBlobClient(config);
        CloudBlobContainer container = this.getContainerForFolder(config, blobClient);
        return container.getBlockBlobReference(location);
    }

    private CloudBlobClient getCloudBlobClient(MediaStorageConfigService.MediaFolderConfig config) throws Exception {
        String connectionString = this.getConnectionString(config);
        CloudStorageAccount account = CloudStorageAccount.parse(connectionString);
        return account.createCloudBlobClient();
    }

    private String getConnectionString(MediaStorageConfigService.MediaFolderConfig config) {
        String connectionString = config.getParameter("connection");
        if (connectionString == null) {
            throw new ExternalStorageServiceException("Windows Azure specific configuration not found [key: connection was empty");
        } else {
            return connectionString;
        }
    }

    private CloudBlobContainer getContainerForFolder(MediaStorageConfigService.MediaFolderConfig config, CloudBlobClient blobClient) throws Exception {
        Integer numRetries = (Integer)config.getParameter("createContainer.numRetries", Integer.class);
        Integer delayInSeconds = (Integer)config.getParameter("createContainer.delayInSeconds", Integer.class);
        int retries = 0;

        while(true) {
            String containerName = AzureCloudUtils.computeContainerAddress(config);

            try {
                CloudBlobContainer container = blobClient.getContainerReference(containerName);
                container.createIfNotExists();
                return container;
            } catch (StorageException var9) {
                if (retries >= numRetries) {
                    throw var9;
                }

                Duration sleepTime = Duration.ofSeconds((long)delayInSeconds);
                LOG.debug("Can't create container. Reason: \"{}\". Retrying for container \"{}\" with settings - num retries: {}, try: {}, delay: {}", new Object[]{var9.getMessage(), containerName, numRetries, retries, delayInSeconds});
                Thread.sleep(sleepTime.toMillis());
                ++retries;
            }
        }
    }

    @Required
    public void setLocationHashService(MediaLocationHashService locationHashService) {
        this.locationHashService = locationHashService;
    }
}
