package de.hybris.training.core.handlers;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Random;

public class StockHandler implements DynamicAttributeHandler<Integer, ProductModel> {

    @Override
    public Integer get(ProductModel model) {
        int minimum  = 100;
        int maximum = 5000;
        Random rand = new Random();
        return minimum + rand.nextInt((maximum - minimum) + 1);
    }

    @Override
    public void set(ProductModel model, Integer stock) {

    }
}
