package de.hybris.training.core.handlers;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Calendar;
import java.util.Date;


public class PastDaysHandler implements DynamicAttributeHandler<Integer, ProductModel> {

    @Override
    public Integer get(ProductModel model) {
        if(model == null || model.getMyCreationDate() == null){
            return 0;
        }
        Long diff = (new Date().getTime() - model.getMyCreationDate().getTime()) / 1000 / 60 / 60 / 24;
        //TODO change it all to make it long as this can possibly lead to an Integer overload.
        return diff.intValue();
    }

    @Override
    public void set(ProductModel model, Integer pastDays) {
        if(model != null && pastDays != null){
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -pastDays);
            model.setMyCreationDate(cal.getTime());
        }
    }
}
