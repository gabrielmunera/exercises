package de.hybris.training.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.training.core.model.process.WishListEmailProcessModel;
import de.hybris.training.core.services.event.WishListEvent;

public class WishListEventListener extends AbstractAcceleratorSiteEventListener<WishListEvent> {

    private ModelService modelService;
    private BusinessProcessService businessProcessService;

    @Override
    protected SiteChannel getSiteChannelForEvent(WishListEvent event) {
        final BaseSiteModel site = event.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
        return site.getChannel();
    }

    @Override
    protected void onSiteEvent(WishListEvent event) {
        WishListEmailProcessModel wishListEmailProcessModel = (WishListEmailProcessModel) getBusinessProcessService().
                createProcess("wishList-" + System.currentTimeMillis(), "wishListEmailProcess");

        wishListEmailProcessModel.setSite(event.getSite());
        wishListEmailProcessModel.setCustomer(event.getCustomer());
        wishListEmailProcessModel.setLanguage(event.getLanguage());
        wishListEmailProcessModel.setCurrency(event.getCurrency());
        wishListEmailProcessModel.setStore(event.getBaseStore());

        wishListEmailProcessModel.setGreeting(event.getGreeting());

        getModelService().save(wishListEmailProcessModel);
        getBusinessProcessService().startProcess(wishListEmailProcessModel);
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }
}
