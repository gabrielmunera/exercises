package de.hybris.training.core.job;

import de.hybris.platform.azure.media.storage.WindowsAzureBlobStorageStrategy;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.media.storage.MediaMetaData;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.storage.impl.StoredMediaData;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.util.CSVConstants;
import de.hybris.training.core.model.ProductCronJobModel;
import de.hybris.training.core.poc.CustomWindowsAzureBlobStorageStrategy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductCronJobPerformable extends AbstractJobPerformable<ProductCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(ProductCronJobPerformable.class);
    private ImportService importService;
    private WindowsAzureBlobStorageStrategy windowsAzureBlobStorageStrategy;
    private CustomWindowsAzureBlobStorageStrategy customAzureBlobStorageStrategy;
    private MediaStorageConfigService mediaStorageConfigService;

    @Override
    public PerformResult perform(ProductCronJobModel productCronJobModel) {
        LOG.info("Starting cron job");

        LOG.info("Starting pushFileToAzure");
        //pushFileToAzure();
        migrateProductMedias("ABC", "jpeg");
        LOG.info("Ended pushFileToAzure");

        ImportConfig importConfig = getImportConfig();
        LOG.info("Performing product's bulk update");
        ImportResult importResult = importService.importData(importConfig);
        if(importResult.isSuccessful()){
            LOG.info("************************");
            LOG.info("***    SUCCESS!!    ****");
            LOG.info("************************");
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }
        LOG.info("************************");
        LOG.info("***     ERROR!!     ****");
        LOG.info("************************");
        return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
    }

    private ImportConfig getImportConfig(){
        ImportConfig importConfig = new ImportConfig();
        LOG.info("Getting impex file");
        File impExFile = getImpExFile("/impEx/bulkProductUpdate.impex");
        LOG.info("Creating impex resource");
        final ImpExResource mediaRes = new StreamBasedImpExResource(new ByteArrayInputStream(getImpExBytes(impExFile)),
                CSVConstants.HYBRIS_ENCODING);
        importConfig.setLegacyMode(Boolean.FALSE);
        importConfig.setSynchronous(true);
        importConfig.setFailOnError(true);
        importConfig.setScript(mediaRes);
        importConfig.setRemoveOnSuccess(false);
        return importConfig;
    }

    private void pushFileToAzure(){
        //final MediaStorageConfigService.MediaFolderConfig rootFolderConfig = mediaStorageConfigService.getConfigForFolder("azureMedias1");
        //final MediaStorageConfigService.MediaFolderConfig rootFolderConfig = mediaStorageConfigService.getConfigForFolder("medias");
        final MediaStorageConfigService.MediaFolderConfig rootFolderConfig = mediaStorageConfigService.getConfigForFolder("poc");
        final String fileName = "test.txt";
        final String mime = "text/plain";
        final String mediaId = "test-poc";
        final byte[] bytes = "Hello azure storage!".getBytes();
        final ByteArrayInputStream mediaStream = new ByteArrayInputStream(bytes);
        /*final Map<String, Object> metaData = buildMediaMetaData(mime, fileName, StringUtils.EMPTY, (long) bytes.length);

        final StoredMediaData result = customAzureBlobStorageStrategy.store(rootFolderConfig, mediaId, metaData, mediaStream);

        LOG.info("Stored media hashForLocation {}, location {} and size {}",
                result.getHashForLocation(),
                result.getLocation(),
                result.getSize());*/

        //mediaStream.close();
    }

    private void migrateProductMedias(String productId, String fileExt){
        String migrationPath = String.format("%s/Downloads/Migration/%s/",
                System.getProperty("user.home"),
                productId);

        List<String> namingConvention = Arrays.asList("30Wx30H","65Wx65H","96Wx96H","284Wx284H","426Wx426H","515Wx515H");
        for(String baseName : namingConvention){
            String fileName = String.format("%s-%s.%s", baseName, productId, fileExt);
            String filePath = migrationPath + fileName;
            final byte[] bytes = getImageBytes(filePath, fileExt);
            pushImageToAzure(bytes, fileName, "images", productId);
        }

    }

    private byte[] getImageBytes(String filePath, String fileExt){
        try {
            BufferedImage image = ImageIO.read(new File(filePath));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(image, fileExt, bos);
            return bos.toByteArray();
        } catch (IOException e){
            throw new IllegalArgumentException("No image found in " + filePath, e);
        }
    }

    private void pushImageToAzure(byte[] bytes, String fileName, String mediaId, String productId){
        final MediaStorageConfigService.MediaFolderConfig rootFolderConfig = mediaStorageConfigService.getConfigForFolder("poc");
        final String mime = "image/jpeg";
        final ByteArrayInputStream mediaStream = new ByteArrayInputStream(bytes);
        final Map<String, Object> metaData = buildMediaMetaData(mime,
                fileName,
                StringUtils.EMPTY,
                (long) bytes.length,
                productId);

        final StoredMediaData result = customAzureBlobStorageStrategy.store(rootFolderConfig, mediaId, metaData, mediaStream);

        LOG.info("Stored media hashForLocation {}, location {} and size {}",
                result.getHashForLocation(),
                result.getLocation(),
                result.getSize());
    }

    private Map<String, Object> buildMediaMetaData(final String mime, final String originalName, final String folderPath,
                                                   final Long size, String productId)
    {
        final Map<String, Object> metaData = new HashMap<>();
        metaData.put(MediaMetaData.MIME, mime);
        metaData.put(MediaMetaData.FILE_NAME, originalName);
        metaData.put(MediaMetaData.FOLDER_PATH, folderPath);
        metaData.put(MediaMetaData.SIZE, size);
        metaData.put("productId", productId);

        return metaData;
    }

    private byte[] getImpExBytes(File impExFile){
        try {
            //TODO Convert the file to string and then add the current date value
            return FileUtils.readFileToByteArray(impExFile);
        }catch (IOException e){
            throw new IllegalArgumentException(String.format("ImpEx resource [%s] does not exists",
                    impExFile.getAbsolutePath()));
        }
    }

    private File getImpExFile(String impExFilePath){
        URL resource = this.getClass().getResource(impExFilePath);
        if (resource == null) {
            throw new IllegalArgumentException(String.format("ImpEx resource [%s] not found", impExFilePath));
        } else {
            try {
                return new File(resource.toURI());
            } catch (URISyntaxException e) {
                throw new IllegalArgumentException(String.format("ImpEx resource [%s] not found", impExFilePath));
            }
        }
    }

    public ImportService getImportService() {
        return importService;
    }

    public void setImportService(ImportService importService) {
        this.importService = importService;
    }

    public WindowsAzureBlobStorageStrategy getWindowsAzureBlobStorageStrategy() {
        return windowsAzureBlobStorageStrategy;
    }

    public void setWindowsAzureBlobStorageStrategy(WindowsAzureBlobStorageStrategy windowsAzureBlobStorageStrategy) {
        this.windowsAzureBlobStorageStrategy = windowsAzureBlobStorageStrategy;
    }

    public MediaStorageConfigService getMediaStorageConfigService() {
        return mediaStorageConfigService;
    }

    public void setMediaStorageConfigService(MediaStorageConfigService mediaStorageConfigService) {
        this.mediaStorageConfigService = mediaStorageConfigService;
    }

    public void setCustomAzureBlobStorageStrategy(CustomWindowsAzureBlobStorageStrategy customAzureBlobStorageStrategy) {
        this.customAzureBlobStorageStrategy = customAzureBlobStorageStrategy;
    }
}
